package com.example.awula3.myapplication.ui;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.awula3.myapplication.R;
import com.example.awula3.myapplication.commons.view.TouchImageView;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.example.awula3.myapplication.ui.FullscreenImageFragment.OnImageLongPressListener} interface
 * to handle interaction events.
 * Use the {@link FullscreenImageFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class FullscreenImageFragment extends Fragment implements View.OnLongClickListener{
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_URL_TAG = "url";

    /**
     * Holds the url as a {@link java.lang.String} that points to the remote image
     */
    private String mImageUrl;

    private OnImageLongPressListener mListener;

    /**
     * Holds a reference to the fullscreen TouchImageView
     */
    private TouchImageView mImageView;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param url A {@link java.net.URL} object containing the url to load.
     * @return A new instance of fragment FullscreenImageFragment.
     */
    public static FullscreenImageFragment newInstance(URL url) {
        FullscreenImageFragment fragment = new FullscreenImageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_URL_TAG, url.toString());
        fragment.setArguments(args);
        return fragment;
    }
    public FullscreenImageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mImageUrl = getArguments().getString(ARG_URL_TAG);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_fullscreen_image, container, false);

        mImageView = (TouchImageView) layout.findViewById(R.id.fullscreenImageView);
        mImageView.setOnLongClickListener(this);

        Picasso.with(mImageView.getContext()).load(mImageUrl).into(mImageView);

        return layout;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnImageLongPressListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnImageLongPressListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onLongClick(View view) {
        if (mListener != null) {
            try {
                mListener.onImageLongPress(new URL(mImageUrl));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    /**
     * A callback that can notify the activity of an image being long pressed
     */
    public interface OnImageLongPressListener {
        public void onImageLongPress(URL url);
    }

}
