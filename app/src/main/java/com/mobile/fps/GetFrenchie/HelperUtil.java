package com.mobile.fps.GetFrenchie;

import android.content.Context;
import android.net.ConnectivityManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class HelperUtil {
    public static <T> String getTAG(Class<T> clazz) {
        String className = clazz.getName();
        className = getClassNameFromPackageName(className);
        return className;
    }

    private static String getClassNameFromPackageName(String className) {
        if (className.contains(".")) {
            String[] classNameArr = className.split("\\.");
            className = classNameArr[classNameArr.length - 1];
        }
        return className;
    }

    public static final String TAG = HelperUtil.class.getSimpleName();

    private static ArrayList<String> frenchBulldogArray;

    private int counter = 0;

    private static boolean isReady = false;

    public static boolean isReady() {
        return isReady;
    }

    public String getURL() {
        String url = "";
        if (frenchBulldogArray != null) {

            if (counter != 100) {
                url = frenchBulldogArray.get(counter);
                counter++;
            }
            return url;
        }
        return null;
    }

    /**
     * Check if device is online or not
     *
     * @return true if it has access to Internet, false otherwise
     */
    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm != null && cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    public static void fetchUrls(View rootView) {
        FlickrSearchAsyncTask flickSearch = new FlickrSearchAsyncTask(frenchBulldogArray);
        flickSearch.execute();
    }

    private static class FlickrSearchAsyncTask extends
            AsyncTask<Void, Void, ArrayList<String>> {

        private ArrayList<String> frenchBulldogArray;

        public FlickrSearchAsyncTask(ArrayList<String> frenchBulldogArray) {
        }

        //runs on UI Thread
        protected void onPreExecute() {
        }

        @Override
        protected ArrayList<String> doInBackground(Void... params) {
            Thread backgroundThread = Thread.currentThread();
            backgroundThread.setName("background_thread");

            String flickrURL = "https://api.flickr.com/services/rest/?" +
                    "text=" +
                    "French%20Bulldog" +
                    "&nojsoncallback=1" +
                    "&format=json" +
                    "&method=flickr.photos.search" +
                    "&api_key=08c70c7e03eeb691b5443c731b3d1a9e";
            try {


                // Parsed the string URL to ensure proper format.
                URL obj = new URL(Uri.parse(flickrURL).toString());

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();


                // optional default is GET
                con.setRequestMethod("GET");

                int responseCode = con.getResponseCode();
                Log.v(TAG, "Sending 'GET' request to URL : " + flickrURL);
                Log.v(TAG, "Response Code:  " + responseCode);

                if (responseCode == 200) {
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            con.getInputStream()));

                    // Returns all the json text
                    String jsonText = readAll(in);
                    Log.v(TAG, "--------------------> JsonText: " + jsonText);

                    JSONObject jsonObj = new JSONObject(jsonText);

                    // Access store all items of Photos
                    JSONArray json_photoArray = jsonObj.getJSONObject("photos")
                            .getJSONArray("photo");

                    // First element in nested array of photos
                    ArrayList<String> urlArray = parseJsonArray(json_photoArray);
                    in.close();
                    return urlArray;

                }

            } catch (MalformedURLException e) {
                Log.v(TAG, "Url can't be parsed");
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;

        }

        //Runs on UI Thread
        protected void onPostExecute(ArrayList<String> result) {
            frenchBulldogArray = result;
            Log.v(TAG, frenchBulldogArray.get(0));
            isReady =true;
        }

        private String readAll(Reader rd) throws IOException {
            StringBuilder sb = new StringBuilder();
            int cp;
            while ((cp = rd.read()) != -1) {
                sb.append((char) cp);
            }
            return sb.toString();
        }

        private ArrayList<String> parseJsonArray(JSONArray array)
                throws JSONException {
            String[] urlArray = new String[array.length()];
            for (int i = 0; i < array.length(); i++) {

                JSONObject j = array.getJSONObject(i);
                String url = "https://c2.staticflickr.com/"
                        + j.getString("farm").toString() + "/"
                        + j.getString("server").toString() + "/"
                        + j.getString("id").toString() + "_"
                        + j.getString("secret").toString() + ".jpg";
                urlArray[i] = url;
            }

            return new ArrayList<String>(Arrays.asList(urlArray));


        }

    }


}
