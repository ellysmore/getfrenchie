package com.mobile.fps.GetFrenchie.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.mobile.fps.GetFrenchie.HelperUtil;
import com.mobile.fps.GetFrenchie.R;
import com.mobile.fps.GetFrenchie.commons.BaseFragment;

public class HomeFragment extends BaseFragment {
    private View mRootView;

    private ImageView mFrenchie;
    private ProgressBar mProgressBar;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_home, container, false);
        mFrenchie = (ImageView) mRootView.findViewById(R.id.frenchie_button);
        mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progress_bar);
        HelperUtil.fetchUrls(mRootView);

        mFrenchie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (HelperUtil.isReady()) {
                    mFrenchie.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.GONE);
                    // TODO: Launch fragment
                } else {
                    mFrenchie.setVisibility(View.GONE);
                    mProgressBar.setVisibility(View.VISIBLE);
                }

            }
        });
        return mRootView;
    }

}
