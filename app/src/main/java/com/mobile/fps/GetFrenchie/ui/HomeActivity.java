package com.mobile.fps.GetFrenchie.ui;

import android.os.Bundle;

import com.mobile.fps.GetFrenchie.R;
import com.mobile.fps.GetFrenchie.commons.BaseActivity;

public class HomeActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(getContainerId(), HomeFragment.newInstance())
                    .commit();
        }
    }

    private int getContainerId() {
        return R.id.container;
    }

}
