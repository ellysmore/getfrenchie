package com.mobile.fps.GetFrenchie;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.mobile.fps.GetFrenchie.ui.HomeActivity;

public class EntryActivity extends Activity {

    public static final String TAG = "EntryActivity";

    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry);
        final Intent launchIntent = new Intent();
        launchIntent.setClass(getApplicationContext(), HomeActivity.class);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(launchIntent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        }, 1000);
    }
}
